from flask import make_response
from flask import jsonify


class CreatedAtLocationResponse:
    def __init__(self, location, content):
        self.__location = location
        self.__content = content

    def make(self):
        response = make_response(jsonify(self.__content))
        response.headers["Location"] = self.__location
        return response, 201


class OkResponse:
    def __init__(self, content=None):
        self.__content = content

    def make(self):

        if self.__content:
            json = jsonify(self.__content)
            response = make_response(json)
        else:
            response = ""

        return response, 200


class BadRequestResponse:
    def __init__(self, error_message=None):
        self.__error_message = error_message

    def make(self):
        if self.__error_message:
            response = make_response(jsonify({"error": self.__error_message}))
        else:
            response = ""

        return response, 400


class InternalServerErrorResponse:
    def __init__(self, error_message=None, exception=None, include_exception_detail=False):
        self.__error_message = error_message
        self.__exception = exception
        self.__include_exception_detail = include_exception_detail

    def make(self):
        content = {}

        if self.__error_message:
            content["error"] = self.__error_message

        if self.__include_exception_detail:
            if self.__exception:
                content["detail"] = str(self.__exception)

        response = make_response(jsonify(content))
        return response, 500
