import pypyodbc


class OdbcDataService:
    def __init__(self, connection_string):
        self.__connection_string = connection_string
        self.__connected = False
        self.__connection = None
        self.__cursor = None

    def execute_command(self, command_text, *command_parameters):
        self.__cursor = self.__connection.cursor()
        self.__cursor.execute(command_text, command_parameters)

    def commit(self):
        self.__cursor.commit()

    def disconnect(self):
        if self.__connection is not None:
            self.__connection.close()

    def connect(self):
        self.__connection = pypyodbc.connect(self.__connection_string)
