from datetime import datetime
from flask import Flask
from flask import request
from api_responses import CreatedAtLocationResponse, OkResponse, BadRequestResponse, InternalServerErrorResponse
from odbc_data_service import OdbcDataService

app = Flask(__name__)
odbc_data_service = OdbcDataService(
    "Driver={SQL Server Native Client 11.0};Server=.;Database=Books;uid=pypy;pwd=Password1$")


@app.route("/api/v1/ping")
def ping():
    return OkResponse({"version": "1.0.0", "ts": int(datetime.utcnow().timestamp())}).make()


@app.route("/api/v1/dbping")
def db_ping():
    try:
        odbc_data_service.connect()
        odbc_data_service.execute_command("SELECT 1")
        print("Connected to database.")
        response = OkResponse().make()
        return response
    except Exception as e:
        response = InternalServerErrorResponse("Could not connect to the database.", e, True).make()
        return response


@app.route("/api/v1/books", methods=["POST"])
def add_book():
    if not request.json:
        response = BadRequestResponse("Only JSON request format is supported.").make()
        return response

    if "isbn" not in request.json or "title" not in request.json:
        response = BadRequestResponse("Request is missing required fields.").make()
        return response

    new_book = {"isbn": request.json["isbn"], "title": request.json["title"]}

    try:
        command_text = "INSERT INTO Book (isbn, title) VALUES (?, ?)"
        __run_and_commit(odbc_data_service, command_text, new_book["isbn"], new_book["title"])
        response = CreatedAtLocationResponse("api/v1/books/{0}".format(new_book["isbn"]), new_book).make()

        return response
    except Exception as e:
        response = InternalServerErrorResponse("Could not add book.", e, True).make()
        return response
    finally:
        odbc_data_service.disconnect()


def __run_and_commit(odbc_data_service, command_text, *command_parameters):
    odbc_data_service.connect()
    odbc_data_service.execute_command(command_text, *command_parameters)
    odbc_data_service.commit()


if __name__ == "__main__":
    app.run(port=5556, debug=True)
